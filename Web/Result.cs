namespace Web
{
    public class Result<TResponse>
    {
        public bool Succeeded { get; set; }
        
        public TResponse Data { get; set; }
    }
}