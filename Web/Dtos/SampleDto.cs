using System.ComponentModel.DataAnnotations;
using MediatR;

namespace Web.Dtos
{
    public class SampleDto : IRequest<string>
    {
        [Required(ErrorMessage = "name is required")]
        public string Name { get; set; }

        [Required]
        public string LastName { get; set; }
        
        [Required]
        public string Gender { get; set; }
    }
}