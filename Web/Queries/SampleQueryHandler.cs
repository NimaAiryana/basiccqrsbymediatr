using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Web.Dtos;

namespace Web.Queries
{
    public class SampleQueryHandler : IRequestHandler<SampleDto, string>
    {
        public async Task<string> Handle(SampleDto request, CancellationToken cancellationToken)
        {
            if (request.Name is "nima")
                throw new Exception("name should not nima");

            await Task.FromResult<int>(0);

            System.Console.WriteLine("samplre response");

            return $"hello {request.Name}";
        }
    }
}