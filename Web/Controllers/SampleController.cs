﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Web.Dtos;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SampleController : ControllerBase
    {
        private readonly IMediator mediator;

        public SampleController(IMediator mediator) => this.mediator = mediator;

        [Route("/api/sample/hello")]
        public async Task<IActionResult> HelloAsync([FromBody] SampleDto dto) =>
            Ok(new Result<string>()
            {
                Succeeded = true,
                Data = await mediator.Send(dto)
            });
    }
}
