using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Web.Behaviors
{
    public class SampleBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            // before
            System.Console.WriteLine("before sample response - behavior");

            var response = await next();

            // after
            System.Console.WriteLine("after sample response - behavior");

            return response;
        }
    }
}